package br.com.droid;

import org.apache.coyote.Response;

import com.sun.org.apache.xerces.internal.util.Status;

public class NoContentException extends WebApplicationException {
	private static final long serialVersionUID = 1L;

	public NoContentException(String mensagem) {
		super(Response.status(Status.NOT_FOUND).entity(mensagem).build());
	}

}
